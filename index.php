<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name; // "shaun"
echo "<br> Legs: " . $sheep->legs; // 4
echo "<br> Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name; 
echo "<br> Legs: " . $sungokong->legs;
echo "<br> Cold Blooded : " . $sungokong->cold_blooded; 

echo $sungokong->yell(); // "Auooo"

$kodok = new Frog("buduk");
echo "Name: " . $kodok->name; 
echo "<br> Legs: " . $kodok->legs;
echo "<br> Cold Blooded : " . $kodok->cold_blooded;
echo $kodok->jump() ; // "hop hop"
?>